# -----------------------------------------------------------------------------
# vincitransducer startup cmd
# -----------------------------------------------------------------------------
require vincitransducer

# -----------------------------------------------------------------------------
# setting parameters when not using auto deployment
# -----------------------------------------------------------------------------
epicsEnvSet(PORTNAME, "PortA")
epicsEnvSet(IPADDR, "192.168.1.32")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(LOCATION, "Utgard; $(IPADDR)")
epicsEnvSet(PREFIX, "utg-vincitransducer-001")
epicsEnvSet(IOCNAME, "utg-vincitransducer-001")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(vincitransducer_DIR)db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
#

# vincitransducer
iocshLoad("$(vincitransducer_DIR)/vincitransducer.iocsh")
# EOF